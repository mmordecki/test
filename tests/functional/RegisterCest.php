<?php

namespace tests\functional;

use Yii;
use yii\helpers\Url;
use app\models\User;

/**
 * Class RegisterCest
 * @package tests\functional
 */
class RegisterCest
{
    const MALE = 'mezczyzna';
    const FEMALE = 'kobieta';

    public function _before(\FunctionalTester $I)
    {
        $I->amOnRoute(Url::toRoute('@register'));
    }

    /**
     * Register Male user.
     *
     * @param \FunctionalTester $I
     */
    public function registerMale(\FunctionalTester $I)
    {
        $userEmail = $this->generateUserEmail(self::MALE);
        $I->amGoingTo('register new user');
        $I->see('Załóż konto');
        $I->see('#userregistrationform-email');
        $I->fillField('#userregistrationform-email', $userEmail);
        $I->fillField('#userregistrationform-email_repeat', $userEmail);
        $I->fillField('#userregistrationform-password', 'Qwerty123!');
        $I->selectOption('#userregistrationform-sex', 'Mężczyzna');
        $I->fillField('#userregistrationform-dob', '19950401');
        $I->checkOption('#userregistrationform-acceptance1_regulations');
        $I->checkOption('#userregistrationform-acceptance2_data_processing');
//        $I->submitForm('#w0', [
//            $I->fillField('#userregistrationform-email', $userEmail),
//            $I->fillField('#userregistrationform-email_repeat', $userEmail),
//            $I->fillField('#userregistrationform-password', 'Qwerty123!'),
//            $I->selectOption('#userregistrationform-sex', 'Mężczyzna'),
//            $I->fillField('#userregistrationform-dob', '19950401'),
//            $I->checkOption('#userregistrationform-acceptance1_regulations'),
//            $I->checkOption('#userregistrationform-acceptance2_data_processing'),
//        ]);
        $I->seeCheckboxIsChecked('#userregistrationform-acceptance1_regulations');
        $I->click('Wyślij');
        $I->canSeeResponseCodeIs(200);
        $I->seeRecord(User::class, ['email' => $userEmail]);
//        $I->amLoggedInAs($userEmail);
//        $I->expectTo('see validations errors');
//        $I->dontSee('To pole nie może być puste.');
//        $I->see('Zgoda wymagana');
//        $I->dontSee('Załóż konto');
//        $I->see('Twoje konto zostało zarejestrowane');
    }

    /**
     * @param string $gender
     * @return string
     */
    private function generateUserEmail(string $gender): string
    {
        return 'test_' . $gender .'_' . date('Y-m-d-H:i') . '@easysoftware.pl';
    }
}
