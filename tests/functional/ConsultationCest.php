<?php

namespace tests\functional;

use Yii;
use yii\helpers\Url;
use app\models\User;

/**
 * Class ConsultationCest
 * @package tests\functional
 */
class ConsultationCest
{
    public function goThroughConsultation(\FunctionalTester $I)
    {
        $I->amOnPage('/lekarstwo/angiletta');
        $I->see('ROZPOCZNIJ KONSULTACJĘ');
        $I->click('ROZPOCZNIJ KONSULTACJĘ');
    }
}
